import 'package:core_bloc/base_view.dart';
import 'package:core_bloc/common/paging/paging_bloc.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_bloc/session/presentation/bloc/session_bloc.dart';
import 'package:core_bloc/session/presentation/bloc/session_bloc_event.dart';
import 'package:core_model/helpers/exception_message.dart';
import 'package:core_model/helpers/localization_extension.dart';
import 'package:core_model/network/exception/network_error_code.dart';
import 'package:core_model/network/exception/network_exception.dart';
import 'package:core_ui/di/paging_setting_di.dart';
import 'package:core_ui/paging/config_ui_paging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BasePaging<
    T,
    E extends PagingEvent,
    S extends BasePageState<T, RequestPagingState<T>>,
    BL extends PagingBloc<T, E, S>> extends StatefulWidget {
  const BasePaging(
      {super.key,
      this.onLoadEvent,
      required this.onGetItem,
      this.listenWhen,
      this.listener,
      this.builderWhen,
      this.onError,
      this.onErrorWithData,
      this.loadNextData = const CurrentLoad(),
      this.fullLoad = const LoadingProgress(),
      this.scrollPhysics,
      this.withDefaultScrollPhysics = true,
      this.customSearchView});

  final E Function(bool isFirstPage, String query)? onLoadEvent;
  final Widget Function(T item, int pos) onGetItem;
  final Widget Function(Exception? exception)? onError;
  final Widget Function(Exception? exception)? onErrorWithData;
  final Widget fullLoad;
  final Widget loadNextData;
  final bool Function(S, S)? listenWhen;
  final void Function(BuildContext, S)? listener;
  final bool Function(S, S)? builderWhen;
  final ScrollPhysics? scrollPhysics;
  final bool withDefaultScrollPhysics;
  final Widget Function(TextEditingController editingController,
      Function(String query) onChange)? customSearchView;

  @override
  State<BasePaging<T, E, S, BL>> createState() =>
      _BasePagingState<T, E, S, BL>();
}

class _BasePagingState<
    T,
    E extends PagingEvent,
    S extends BasePageState<T, RequestPagingState<T>>,
    BL extends PagingBloc<T, E, S>> extends State<BasePaging<T, E, S, BL>> {
  ScrollController controller = ScrollController();
  final TextEditingController editingController = TextEditingController();

  void _onScroll() {
    double maxScroll = controller.position.maxScrollExtent;
    double currentScroll = controller.position.pixels;

    if (currentScroll == maxScroll) {
      _loadEvent(query: editingController.text);
    }
  }

  void _loadEvent({bool isFirstPage = false, String query = ""}) {
    final bloc = BlocProvider.of<BL>(context);
    if (widget.onLoadEvent != null) {
      bloc.add(widget.onLoadEvent!.call(isFirstPage, query));
    } else {
      if (E is PagingEvent) {
        bloc.add(PagingEvent(page: isFirstPage ? 1 : null, query: query) as E);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    controller.addListener(_onScroll);
    return BlocConsumer<BL, S>(
      listenWhen: widget.listenWhen ??
          (previous, current) => previous.requestState != current.requestState,
      listener: (context, state) {
        widget.listener?.call(context, state);
        if (state.requestState?.requestStatus == RequestStatus.failure) {
          if (state.requestState?.exception is NetworkException) {
            var networkEx = state.requestState?.exception as NetworkException;
            if (networkEx.code == NetworkErrorCode.unauthorized) {
              String message =
                  state.requestState!.exception!.getMessage(context);
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.red,
                behavior: SnackBarBehavior.floating,
                dismissDirection: DismissDirection.up,
                content: Text(message),
              ));
              context.read<SessionBloc>().add(OnCheckSession());
            }
          }
        }
      },
      buildWhen: widget.builderWhen,
      builder: (context, state) {
        if (state.requestState != null) {
          if (!(state is SearchPagingMixin)) {
            return _getUi(state.requestState!, state.hasReachedMax);
          }
          return _withSearchView(state.requestState!, state.hasReachedMax);
        } else {
          return Container();
        }
      },
    );
  }

  void _onChangeSearch(String query) {
    _loadEvent(isFirstPage: true, query: query);
  }

  Widget _withSearchView(RequestPagingState<T> loaded, bool hasReachedMax) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 8.0),
          child: widget.customSearchView != null
              ? widget.customSearchView!
                  .call(editingController, _onChangeSearch)
              : TextField(
                  onChanged: _onChangeSearch,
                  controller: editingController,
                  decoration: const InputDecoration(
                      labelText: "Search",
                      hintText: "Search",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(8.0)))),
                ),
        ),
        Expanded(child: _getUi(loaded, hasReachedMax)),
      ],
    );
  }

  @override
  void dispose() {
    controller.removeListener(_onScroll);
    super.dispose();
  }

  Widget _getUi(RequestPagingState<T> loaded, bool hasReachedMax) {
    switch (loaded.requestStatus) {
      case RequestStatus.loading:
        return widget.fullLoad;
      case RequestStatus.success:
        if (loaded.sizeData > 0) {
          return _createListView(loaded, hasReachedMax);
        }
        return _onEmptyPage();
      case RequestStatus.failure:
        if (loaded.sizeData > 0) {
          return _createListView(loaded, hasReachedMax);
        } else {
          return widget.onError != null
              ? widget.onError?.call(loaded.exception)
              : _onErrorPage(loaded.exception);
        }
      default:
        return Container();
    }
  }

  _onErrorPage(Exception? ex) {
    final uiConfig = PagingSettingInjection.get<ConfigUiPaging>();
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: uiConfig != null ? 80 : 16,
              width: uiConfig != null ? 80 : 0,
              child: uiConfig != null
                  ? Image(
                      image: uiConfig.baseLogoImage!,
                      height: 80,
                      width: 80,
                      fit: BoxFit.cover,
                    )
                  : null,
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              ex.getMessage(context),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.red,
              ),
            ),
            IconButton(
              padding: EdgeInsets.zero,
              onPressed: () => _loadEvent(query: editingController.text),
              iconSize: 36,
              alignment: Alignment.center,
              icon: const Icon(
                Icons.refresh,
                color: Colors.black87,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _onEmptyPage() {
    final uiConfig = PagingSettingInjection.get<ConfigUiPaging>();
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: uiConfig != null ? 80 : 16,
              width: uiConfig != null ? 80 : 0,
              child: uiConfig != null
                  ? Image(
                      image: uiConfig.baseLogoImage!,
                      height: 80,
                      width: 80,
                      fit: BoxFit.cover,
                    )
                  : null,
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              context.getAppLocalizations().empty_data,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 16),
            ),
            IconButton(
              padding: EdgeInsets.zero,
              onPressed: () =>
                  _loadEvent(isFirstPage: true, query: editingController.text),
              iconSize: 36,
              alignment: Alignment.center,
              icon: const Icon(
                Icons.refresh,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _createListView(RequestPagingState<T> loaded, bool hasReachedMax) {
    return RefreshIndicator(
      onRefresh: () async {
        _loadEvent(isFirstPage: true);
        editingController.clear();
      },
      child: ListView.builder(
        controller: controller,
        physics: widget.withDefaultScrollPhysics
            ? const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics())
            : widget.scrollPhysics,
        itemCount: (hasReachedMax) ? loaded.sizeData : loaded.sizeData + 1,
        itemBuilder: (context, index) => (index < loaded.sizeData)
            ? widget.onGetItem(loaded.data[index], index)
            : _checkOnExtraLine(loaded.requestStatus, loaded.exception),
      ),
    );
  }

  Widget _checkOnExtraLine(RequestStatus requestStatus, Exception? ex) {
    if (requestStatus == RequestStatus.success) {
      return widget.loadNextData;
    } else if (requestStatus == RequestStatus.failure) {
      if (widget.onErrorWithData != null) {
        return widget.onErrorWithData!.call(ex);
      } else {
        return ErrorNextData(
          ex.getMessage(context),
          reload: () {
            _loadEvent(query: editingController.text);
          },
        );
      }
    } else {
      return Container();
    }
  }
}

class CurrentLoad extends StatelessWidget {
  const CurrentLoad({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SizedBox(
        width: 30,
        height: 30,
        child: CircularProgressIndicator(),
      ),
    );
  }
}

class ErrorNextData extends StatelessWidget {
  ErrorNextData(this.message, {super.key, required this.reload});
  final String message;
  final Function() reload;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 24,
      ),
      child: Row(
        children: [
          const Icon(
            Icons.error_outline_outlined,
            color: Colors.red,
            size: 24,
          ),
          const SizedBox(width: 8),
          Expanded(
            child: Text(
              message,
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.red),
            ),
          ),
          IconButton(
            onPressed: reload,
            icon: const Icon(
              Icons.refresh_outlined,
              color: Colors.grey,
              size: 24,
            ),
          ),
        ],
      ),
    );
  }
}
