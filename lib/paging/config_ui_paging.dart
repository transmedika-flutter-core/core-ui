import 'package:flutter/material.dart';

abstract class ConfigUiPaging {
  abstract final AssetImage? baseLogoImage;
}

class BaseConfigUiPaging extends ConfigUiPaging {
  AssetImage? get baseLogoImage => null;
}
