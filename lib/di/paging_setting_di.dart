import 'package:core_ui/paging/config_ui_paging.dart';
import 'package:get_it/get_it.dart';

class PagingSettingInjection {
  static final GetIt _getIt = GetIt.instance;

  static void registerDependencies({required ConfigUiPaging uiPaging}) {
    _getIt.registerSingleton<ConfigUiPaging>(uiPaging);
  }

  static void unRegisterDependencies() {
    _getIt.unregister<ConfigUiPaging>();
  }

  static T? get<T extends Object>() {
    if (_getIt.isRegistered<T>()) {
      return _getIt.get<T>();
    }
    return null;
  }
}
