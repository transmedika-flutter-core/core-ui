import 'package:core_ui/navigator/transitions.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class NestedNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigationKey;
  final String initialRoute;
  final Map<String, WidgetBuilder> routes;
  final Function(String? route)? onPop;

  const NestedNavigator({super.key,
    required this.navigationKey,
    required this.initialRoute,
    required this.routes,
    this.onPop
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Navigator(
        key: navigationKey,
        initialRoute: initialRoute,
        onGenerateRoute: (RouteSettings routeSettings) {
          WidgetBuilder builder = routes[routeSettings.name]!;
          if (routeSettings.name == initialRoute) {
            return MaterialPageRoute(
              builder: builder,
              settings: routeSettings
            );
          } else {
            return PageRouteBuilder(
              pageBuilder: (context, __, ___) => builder(context),
              settings: routeSettings,
              transitionsBuilder: (context, animation, secondaryAnimation, child) {
                return mySlideTransition(
                  context, animation, secondaryAnimation, child
                );
              },
              transitionDuration: const Duration(milliseconds: 500)
            );
          }
        },
      ),
      onWillPop: () async  {
        onPop?.call(ModalRoute.of(context)?.settings.name);
        if(await navigationKey.currentState?.maybePop() == true){
          return Future<bool>.value(false);
        }else{
          return Future<bool>.value(true);
        }
        /*if(navigationKey.currentState?.canPop() == true) {
          navigationKey.currentState?.maybePop();
          return Future<bool>.value(false);
        }
        return Future<bool>.value(true);*/
      },
    );
  }
}